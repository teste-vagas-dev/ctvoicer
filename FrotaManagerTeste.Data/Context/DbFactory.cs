﻿using System;
using Microsoft.EntityFrameworkCore;

namespace FrotaManagerTeste.Data.Context
{
    public class DbFactory : IDisposable   
    {   
        private bool _disposed;   
        private readonly Func<StoreContext> _instanceFunc;   
        private DbContext _dbContext;   
        public DbContext DbContext => _dbContext ??= _instanceFunc.Invoke();   
   
        public DbFactory(Func<StoreContext> dbContextFactory)   
        {   
            _instanceFunc = dbContextFactory;   
        }

        public void Dispose()
        {
            if (_disposed || _dbContext == null)
            {
                return;
            }
            
            _disposed = true;   
            _dbContext.Dispose();
        }   
    } 
}

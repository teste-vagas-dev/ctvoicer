using FrotaManagerTeste.Data.Mapping;
using FrotaManagerTeste.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace FrotaManagerTeste.Data.Context
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
        }
        
        public DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Vehicle>(new VehicleMap().Configure);
        }
    }
}
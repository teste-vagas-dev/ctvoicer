using FrotaManagerTeste.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FrotaManagerTeste.Data.Mapping
{
    public class VehicleMap : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable("Vehicle");

            builder.HasKey(p => p.Id);

            builder.HasIndex(p => p.Chassis)
                .IsUnique();

            builder.Property(p => p.Chassis)
                .HasMaxLength(17);
        }
    }
}
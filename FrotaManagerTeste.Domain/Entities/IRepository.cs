using System.Linq;
using System.Threading.Tasks;

namespace FrotaManagerTeste.Domain.Entities
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> Query();
        
        void Insert(TEntity obj);

        void Update(TEntity obj);

        void Delete(int id);
        
        Task SaveChangesAsync();
    }
}
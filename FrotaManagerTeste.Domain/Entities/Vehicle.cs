namespace FrotaManagerTeste.Domain.Entities
{
    public class Vehicle : Entity
    {
        public string Chassis { get; set; }
        public VehicleType VehicleType { get; set; }
        public byte NumberOfPassengers { get; set; }
        public string Color { get; set; }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using FrotaManagerTeste.Domain.Entities;

namespace FrotaManagerTeste.Services
{
    public interface IVehicleService
    {
        Task<Vehicle> GetVehicleByIdAsync(int vehicleId);
        Task<Vehicle> UpsertVehicleAsync(Vehicle vehicle);
        Task<IList<Vehicle>> GetLastVehiclesAsync();
        Task<IList<Vehicle>> SearchForVehiclesAsync(string search);
        Task<bool> IsChassisAlreadyAdd(string chassis, int vehicleId = 0);
        Task RemoveVehicleAsync(int vehicleId);
    }
}
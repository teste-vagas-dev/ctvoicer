using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrotaManagerTeste.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace FrotaManagerTeste.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IRepository<Vehicle> _vehicleRepository;

        public VehicleService(IRepository<Vehicle> vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        
        // Insert and Update of all kind of vehicles
        public async Task<Vehicle> UpsertVehicleAsync(Vehicle vehicle)
        {
            if (vehicle.Id == 0)
            {
                _vehicleRepository.Insert(vehicle);
            }
            else
            {
                _vehicleRepository.Update(vehicle);
            }

            vehicle.Chassis = vehicle.Chassis.ToLower();
            vehicle.NumberOfPassengers = (byte) (vehicle.VehicleType == VehicleType.Truck ? 2 : 42);
            
            await _vehicleRepository.SaveChangesAsync();
            
            return vehicle;
        }
        
        // Get vehicles by Id
        public async Task<Vehicle> GetVehicleByIdAsync(int vehicleId)
        {
            return await _vehicleRepository.Query().FirstOrDefaultAsync(p => p.Id == vehicleId);
        }

        // Check if chassis is already added in the store
        public async Task<bool> IsChassisAlreadyAdd(string chassis, int vehicleId = 0)
        {
            var query = _vehicleRepository.Query().AsQueryable();
            if (vehicleId != 0)
            {
                query = query.Where(p => p.Id != vehicleId);
            }

            return await query.AnyAsync(p => p.Chassis == chassis.ToLower());
        }
        
        // Get all last vehicles add in the store
        public async Task<IList<Vehicle>> GetLastVehiclesAsync()
        {
            var vehicles = await _vehicleRepository.Query().OrderByDescending(p => p.Id).ToListAsync();
            return vehicles;
        }

        // Search for vehicles in the store
        public async Task<IList<Vehicle>> SearchForVehiclesAsync(string search)
        {
            var vehicles = await _vehicleRepository.Query().Where(p => p.Chassis.Contains(search.ToLower())).ToListAsync();
            return vehicles;
        }

        // Remove vehicles in the store by id
        public async Task RemoveVehicleAsync(int vehicleId)
        {
            var vehicle = await _vehicleRepository.Query().FirstOrDefaultAsync(p => p.Id == vehicleId);
            if (vehicle != null)
            {
                _vehicleRepository.Delete(vehicleId);
                await _vehicleRepository.SaveChangesAsync();
            }
        }
    }
}
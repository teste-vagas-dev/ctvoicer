using System;
using System.Linq;
using System.Threading.Tasks;
using FrotaManagerTeste.Data.Context;
using FrotaManagerTeste.Data.Repository;
using FrotaManagerTeste.Domain.Entities;
using FrotaManagerTeste.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace FrotaManagerTeste.Tests
{
    
    public class VehicleServiceTests
    {
        private IVehicleService _vehicleService;
        private const string NewChassis1 = "20545-25455-5668-5452";
        private const string NewChassis2 = "black-25452-ship-pirate-444";

        [SetUp]
        public async Task Setup()
        {
            var services = new ServiceCollection();
            
            services.AddDbContext<StoreContext>(options =>
            {
                options.UseInMemoryDatabase("FrotaManager");
            });
            
            services.AddScoped<DbFactory>();
            services.AddScoped<Func<StoreContext>>(provider => provider.GetService<StoreContext>);
            
            services.AddTransient<IVehicleService, VehicleService>();
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            
            var serviceProvider = services.BuildServiceProvider();
            _vehicleService = serviceProvider.GetService<IVehicleService>();

            await PopulateDb();
        }

        private async Task PopulateDb()
        {
            var vehicle1 = new Vehicle
            {
                Chassis = NewChassis1,
                Color = "Azul",
                VehicleType = VehicleType.Bus
            };
            
            var vehicle2 = new Vehicle
            {
                Chassis = NewChassis2,
                Color = "Vermelho",
                VehicleType = VehicleType.Truck
            };
            
            await _vehicleService.UpsertVehicleAsync(vehicle1);
            await _vehicleService.UpsertVehicleAsync(vehicle2);
        }
        
        [Theory]
        public async Task CheckForVehiclesAdded()
        {
            var isChassis1Added = await  _vehicleService.IsChassisAlreadyAdd(NewChassis1);
            Assert.AreEqual(isChassis1Added, true);
            
            var isChassis2Added = await  _vehicleService.IsChassisAlreadyAdd(NewChassis2);
            Assert.AreEqual(isChassis2Added, true);
            
            var isChassisFakeAdded = await  _vehicleService.IsChassisAlreadyAdd("new-fake-chassis-4585");
            Assert.AreEqual(isChassisFakeAdded, false);
        }

        [Theory]
        public async Task CheckForChassisSearch()
        {
            var vehicles = await _vehicleService.SearchForVehiclesAsync(NewChassis1);
            Assert.AreEqual(vehicles.Count, 1);
        }

        [Theory]
        public async Task CheckForNumberOfPassengers()
        {
            var vehicles = await _vehicleService.SearchForVehiclesAsync(NewChassis1);
            var vehicle1 = vehicles.FirstOrDefault();

            if (vehicle1 != null)
            {
                Assert.AreEqual(vehicle1.NumberOfPassengers, 42);
            }
            else
            {
                Assert.Fail("vehicle1 not found!");
            }
        }
    }
}
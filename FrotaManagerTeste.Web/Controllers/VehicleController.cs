using System.Collections.Generic;
using System.Threading.Tasks;
using FrotaManagerTeste.Domain.Entities;
using FrotaManagerTeste.Services;
using FrotaManagerTeste.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace FrotaManagerTeste.Web.Controllers
{
    [Route("veiculos")]
    public class VehicleController : Controller
    {
        private IVehicleService _vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        [HttpGet("{query?}")]
        public async Task<IActionResult> Index(string query)
        {
            IList<Vehicle> vehicles;
            if (string.IsNullOrEmpty(query?.Trim()))
            {
                vehicles = await _vehicleService.GetLastVehiclesAsync();
            }
            else
            {
                vehicles = await _vehicleService.SearchForVehiclesAsync(query);
            }

            return View(vehicles);
        }
        
        [HttpGet]
        [Route("novo")]
        public IActionResult AddVehicle()
        {
            return View();
        }

        [HttpPost]
        [Route("novo")]
        public async Task<IActionResult> AddVehicle(VehicleAddViewModel request)
        {
            if (request?.Chassis != null)
            {
                var chassisExists = await _vehicleService.IsChassisAlreadyAdd(request.Chassis);
                if (chassisExists)
                {
                    ModelState.AddModelError("Chassis", "O Chassis já está sendo utilizado.");
                }
            }
            
            if (!ModelState.IsValid)
            {
                return View(request);
            }
            
            await _vehicleService.UpsertVehicleAsync(new Vehicle
            {
                Chassis = request.Chassis,
                Color = request.Color,
                VehicleType = request.VehicleType
            });

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        [Route("editar/{vehicleId:int}")]
        public async Task<IActionResult> EditVehicle(int vehicleId)
        {
            var vehicle = await _vehicleService.GetVehicleByIdAsync(vehicleId);
            if (vehicle == null)
            {
                return NotFound();
            }
            
            return View(vehicle);
        }
        
        [HttpPost]
        [Route("editar/{vehicleId:int}")]
        public async Task<IActionResult> EditVehicle(VehicleEditViewModel request)
        {
            var vehicle = await _vehicleService.GetVehicleByIdAsync(request.Id);
            if (vehicle == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return View(vehicle);
            }

            vehicle.Color = request.Color;
            await _vehicleService.UpsertVehicleAsync(vehicle);
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveVehicle(VehicleEditViewModel request)
        {
            var vehicle = await _vehicleService.GetVehicleByIdAsync(request.Id);
            if (vehicle == null)
            {
                return NotFound();
            }
            
            await _vehicleService.RemoveVehicleAsync(request.Id);
            
            return RedirectToAction("Index");
        }
    }
}
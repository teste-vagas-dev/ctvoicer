﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FrotaManagerTeste.Web.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Chassis = table.Column<string>(type: "TEXT", maxLength: 17, nullable: true),
                    VehicleType = table.Column<int>(type: "INTEGER", nullable: false),
                    NumberOfPassengers = table.Column<byte>(type: "INTEGER", nullable: false),
                    Color = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_Chassis",
                table: "Vehicle",
                column: "Chassis",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vehicle");
        }
    }
}

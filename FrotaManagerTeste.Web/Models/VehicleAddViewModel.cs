using System.ComponentModel.DataAnnotations;
using FrotaManagerTeste.Domain.Entities;

namespace FrotaManagerTeste.Web.Models
{
    public class VehicleAddViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Informe o número do Chassi")]
        public string Chassis { get; set; }
        
        public VehicleType VehicleType { get; set; }
        
        [Required(ErrorMessage = "Informe a cor do veículo")]
        public string Color { get; set; }
    }
}
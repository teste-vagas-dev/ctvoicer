using System.ComponentModel.DataAnnotations;
using FrotaManagerTeste.Domain.Entities;

namespace FrotaManagerTeste.Web.Models
{
    public class VehicleEditViewModel
    {
        public int Id { get; set; }
        
        public string Chassis { get; set; }
        
        public VehicleType VehicleType { get; set; }
        
        [Required(ErrorMessage = "Informe a cor do veículo")]
        public string Color { get; set; }
    }
}
using System;
using FrotaManagerTeste.Data.Context;
using FrotaManagerTeste.Data.Repository;
using FrotaManagerTeste.Domain.Entities;
using FrotaManagerTeste.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FrotaManagerTeste.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            services.AddDbContext<StoreContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection"), opt =>
                {
                    opt.MigrationsAssembly("FrotaManagerTeste.Web");
                });
            });
            
            services.AddScoped<DbFactory>();
            services.AddScoped<Func<StoreContext>>(provider => provider.GetService<StoreContext>);
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IVehicleService, VehicleService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}